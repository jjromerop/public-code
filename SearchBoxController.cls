public class SearchBoxController {

    private static Set<Id> accountIds = new Set<Id>();

    /**
     * @name - loadCaseData
     * @param - String recordId, Case record Id
     * @description - Initialize the required Case values on component init
     * @returnType - RecordResult
    */
    @AuraEnabled
    public static RecordResult loadCaseData(String recordId) {
        RecordResult recResult = new RecordResult();
        Case caseRecord = [SELECT Id, SuppliedName, SuppliedEmail, AccountId, ContactId, Status 
                            FROM Case 
                            WHERE Id = :recordId 
                            LIMIT 1];
        recResult.caseRecord = caseRecord;
        if (recResult.caseRecord.Status == 'Close' || recResult.caseRecord.Status == 'Duplicated') {
            recResult.showSearch = false;
        } else {
            recResult.showSearch = true;
        }
        
        return recResult;
    }

    /**
     * @name - fetchRecords
     * @param - String recordId, Case record Id
     * @description - Initialize the search from the current Case
     * @returnType - RecordResult
    */
    @AuraEnabled
    public static RecordResult fetchRecords(String recordId) {
        RecordResult recResult = new RecordResult();
        Case caseRecord = [SELECT Id, SuppliedName, SuppliedEmail, AccountId, ContactId, Status 
                            FROM Case 
                            WHERE Id = :recordId 
                            LIMIT 1];
        recResult.caseRecord = caseRecord;
        if (recResult.caseRecord.Status == 'Close' || recResult.caseRecord.Status == 'Duplicated') {
            recResult.showSearch = false;
        } else {
            recResult.showSearch = true;
        }
        System.debug('caseRecord: ' + caseRecord);
        if (caseRecord != null && String.isNotEmpty(caseRecord.SuppliedName) && String.isNotEmpty(caseRecord.SuppliedEmail)) {
            getDataByName(caseRecord.SuppliedName, recResult);
            getDataByEmail(caseRecord.SuppliedEmail, recResult);
            if (accountIds != null && !accountIds.isEmpty()) {
                getOpportunities(recResult);
                getCases(recResult);
            }
        }
        return recResult;
    }
    
    /**
     * @name - getDataByName
     * @param - String name
     * @param - RecordResult recResult
     * @description - Search on Account and Lead for records with the same name that parameter indicates
     * @returnType - None
    */
    private static void getDataByName(String name, RecordResult recResult) {

        String nameToSearch = String.escapeSingleQuotes(name);
        List<List<sObject>> searchNameList =
        [FIND :nameToSearch
            IN NAME FIELDS
                RETURNING 
                    Account(Id, Name, PersonEmail, PersonHomePhone, PersonMobilePhone, PersonContactId, Owner.Name),
                    Lead(Id, Name, Email, Phone, Paquete__c, Owner.Name)
                LIMIT 50];

        Account[] searchAccounts = (Account[]) searchNameList[0];
        Lead[] searchLeads = (Lead[]) searchNameList[1];
        
        if (searchAccounts != null && searchAccounts.size() > 0) {
            for (Account acc : searchAccounts) {
                if (recResult.caseRecord.AccountId != acc.Id) {
                    recResult.accountList.add(acc);
                    accountIds.add(acc.Id);
                }
            }
        }

        if (searchLeads != null && searchLeads.size() > 0) {
            for (Lead leadRecord : searchLeads) {
                recResult.leadList.add(leadRecord);
            }
        }
    }

    /**
     * @name - getDataByName
     * @param - String email
     * @param - RecordResult recResult
     * @description - Search on Account and Lead for records with the same email that parameter indicates
     * @returnType - None
    */
    private static void getDataByEmail(String email, RecordResult recResult) {

        String emailToSearch = String.escapeSingleQuotes(email);

        List<List<sObject>> searchEmailList =
        [FIND :emailToSearch
            IN EMAIL FIELDS
                RETURNING 
                    Account(Id, Name, PersonEmail, PersonHomePhone, PersonMobilePhone, PersonContactId, Owner.Name),
                    Lead(Id, Name, Email, Phone, Paquete__c, Owner.Name)
                LIMIT 50];

        Account[] searchAccounts = (Account[]) searchEmailList[0];
        Lead[] searchLeads = (Lead[]) searchEmailList[1];
        
        if (searchAccounts != null && searchAccounts.size() > 0) {
            for (Account acc : searchAccounts) {
                if (recResult.caseRecord.AccountId != acc.Id) {
                    recResult.accountList.add(acc);
                    accountIds.add(acc.Id);
                }
            }
        }

        if (searchLeads != null && searchLeads.size() > 0) {
            for (Lead leadRecord : searchLeads) {
                recResult.leadList.add(leadRecord);
            }
        }
    }

    /**
     * @name - getOpportunities
     * @param - RecordResult recResult
     * @description - Find all Opportunites for the recResult.Accounts
     * @returnType - None
    */
    private static void getOpportunities(RecordResult recResult) {

        for (Opportunity opp : [SELECT Id, Account.Name, Name, StageName, CreatedDate, IsClosed, Owner.Name
                                    FROM Opportunity 
                                    WHERE AccountId IN :accountIds
                                        AND StageName != 'Duplicated'
                                ]) {

            if (opp.IsClosed) {
                recResult.closedOpportunitiesList.add(opp);
            } else {
                recResult.openOpportunitiesList.add(opp);
            }
        }
    }

    /**
     * @name - getCases
     * @param - RecordResult recResult
     * @description - Find all Cases for the recResult.Accounts
     * @returnType - None
    */
    private static void getCases(RecordResult recResult) {
        System.debug('accountIds: ' + accountIds);

        List<Case> caseList = [SELECT Id, Account.Name, Status, CaseNumber, CreatedDate, Owner.Name
                                FROM Case 
                                WHERE AccountId IN :accountIds
                                AND Status NOT IN ('Duplicated', 'Close')];
        recResult.caseList = caseList;
    }

    /**
     * @name - assignAccountToCase
     * @param - String caseId
     * @param - String accountId
     * @param - String contactId
     * @description - Assign an Account and Contact (Optional) to a Case
     * @returnType - Boolean
    */
    @AuraEnabled
    public static Boolean assignAccountToCase(String caseId, String accountId, String contactId) {

        try {
            
            Case caseRecord = new Case();
            caseRecord.Id = caseId;
            caseRecord.AccountId = accountId;

            if (String.isEmpty(contactId)) {
                caseRecord.ContactId = contactId;
            }
            update caseRecord;
            return true;
        } catch (Exception ex) {
            System.debug('Error: ' + ex);
            throw new AuraHandledException('Error saving Case.');
        }
    }

    /**
     * @name - relateCaseAndClose
     * @param - String caseOriginId
     * @param - String caseToRelateId
     * @description - Relates two similar Cases in order to indicate that the Second one is duplicated
     * @returnType - Boolean
    */
    @AuraEnabled
    public static Boolean relateCaseAndClose(String caseOriginId, String caseToRelateId) {

        try {
            Case caseToRelate = new Case();
            caseToRelate.Id = caseToRelateId;
            caseToRelate.ParentId = caseOriginId;
            caseToRelate.Status = 'Duplicated';

            System.debug('caseToRelate: ' + caseToRelate);

            update caseToRelate;
            return true;
        } catch (Exception ex) {
            System.debug('Error: ' + ex);
            throw new AuraHandledException('Error saving Case.');
        }
    }

    /**
     * @name - relateOpportunityAndClose
     * @param - String oppId
     * @param - String caseToRelateId
     * @description - Relates a case to an Opportunity and move to the Duplicated Stage
     * @returnType - Boolean
    */
    @AuraEnabled
    public static Boolean relateOpportunityAndClose(String oppId, String caseToRelateId) {

        try {
            Opportunity oppRecord = new Opportunity();
            oppRecord.Id = oppId;
            oppRecord.Lookupcaso__c = caseToRelateId;
            oppRecord.StageName = 'Duplicated';

            update oppRecord;
            return true;
        } catch (Exception ex) {
            System.debug('Error: ' + ex);
            throw new AuraHandledException('Error saving Opportunity.');
        }
    }

    /** 
      * Object Result
     */
    public class RecordResult {
        
        @AuraEnabled public List<Account> accountList;
        @AuraEnabled public List<Lead> leadList;
        @AuraEnabled public List<Case> caseList;
        @AuraEnabled public List<Opportunity> openOpportunitiesList;
        @AuraEnabled public List<Opportunity> closedOpportunitiesList;
        @AuraEnabled public Case caseRecord;
        @AuraEnabled public Boolean showSearch;

        public RecordResult () {
            accountList = new List<Account>();
            leadList = new List<Lead>();
            caseRecord = new Case();
            caseList = new List<Case>();
            openOpportunitiesList = new List<Opportunity>();
            closedOpportunitiesList = new List<Opportunity>();
        }
    }
}
