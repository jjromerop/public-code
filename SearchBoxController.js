({
    doInit : function(component, event, helper) {
        helper.onLoadSearch(component, event);
    },
    search : function(component, event, helper) {
        helper.search(component, event);
    },
    assignAccount : function(component, event, helper) {
        var recordResult = component.get("v.recordResult");
        var actionResult = false;
        var index = event.getSource().get("v.name");

        if (!$A.util.isEmpty(recordResult.caseRecord.AccountId)) {
            actionResult = confirm("This case has already an Account. Are you sure that you want to change it?");
        } else {
            actionResult = true;
        }

        if (actionResult) {
            helper.assignAccount(component, index);
        }
    },
    relateCase : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        helper.relateCase(component, index);
    },
    relateOpportunity : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        helper.relateOpportunity(component, index);
    },
})
