({
    onLoadSearch : function(component, event) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.loadCaseData");
        action.setParams({ "recordId" : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.showSearch", result.showSearch);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    search : function(component, event) {
        var recordId = component.get("v.recordId");
        this.showHideSpinner(component);
        var action = component.get("c.fetchRecords");
        action.setParams({ "recordId" : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result.accountList.length > 0) {
                    var customerTable = component.find("CustomerTable");
                    $A.util.removeClass(customerTable, "slds-hide");
                }
                if (result.leadList.length > 0) {
                    var leadTable = component.find("LeadsTable");
                    $A.util.removeClass(leadTable, "slds-hide");
                }
                if (result.caseList.length > 0) {
                    var caseTable = component.find("CasesTable");
                    $A.util.removeClass(caseTable, "slds-hide");
                }
                if (result.openOpportunitiesList.length > 0) {
                    var oppOpenTable = component.find("OppsOpenTable");
                    $A.util.removeClass(oppOpenTable, "slds-hide");
                }
                if (result.closedOpportunitiesList.length > 0) {
                    var oppClosedTable = component.find("OppsClosedTable");
                    $A.util.removeClass(oppClosedTable, "slds-hide");
                }
                component.set("v.recordResult", result);
                component.set("v.showSearch", result.showSearch);
                this.showHideSpinner(component);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    assignAccount : function(component, index) {
        var recordResult = component.get("v.recordResult");
        var caseId = recordResult.caseRecord.Id;
        var accountRecord = recordResult.accountList[index];
        var accountId = accountRecord.Id;
        var contactId = accountRecord.PersonContactId;
        var action = component.get("c.assignAccountToCase");
        this.showHideSpinner(component);
        action.setParams({ "caseId" : caseId, "accountId" : accountId, "contactId" : contactId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result) {
                    window.location.reload();
                }
                this.showHideSpinner(component);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    relateCase : function(component, index) {
        var recordResult = component.get("v.recordResult");
        var caseId = recordResult.caseRecord.Id;
        var caseToRecord = recordResult.caseList[index];
        var caseToRelateId = caseToRecord.Id;

        this.showHideSpinner(component);

        var action = component.get("c.relateCaseAndClose");
        action.setParams({ "caseOriginId" : caseId, "caseToRelateId" : caseToRelateId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("result: " + result);
                if (result) {
                    window.location.reload();
                }
                this.showHideSpinner(component);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    relateOpportunity : function(component, index) {
        var recordResult = component.get("v.recordResult");
        var caseId = recordResult.caseRecord.Id;
        var oppToRecord = recordResult.openOpportunitiesList[index];
        var opportunityId = oppToRecord.Id;

        this.showHideSpinner(component);

        var action = component.get("c.relateOpportunityAndClose");
        action.setParams({ "oppId" : opportunityId, "caseToRelateId" : caseId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("result: " + result);
                if (result) {
                    window.location.reload();
                }
                this.showHideSpinner(component);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    showHideSpinner : function(component) {
        var spinner = component.find("spinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
})
